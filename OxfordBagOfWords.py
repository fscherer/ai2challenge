import os
import sys
import time

import gensim

import numpy as np

import theano
import theano.tensor as T


def getVec4Word(model, word):
    # returns vector from glove model
    try:
        vec = model[word]
    except:
        vec = np.zeros(model.vector_size)
    return vec



def readTrainingData(model, path = "../Data/training_set.tsv", rmStopWords = True):

    ids = []

    q = []
    a = []
    y = []

    if (rmStopWords):
        stopWords = open("../Data/StopWords.txt").read().splitlines()
    else:
        stopWords = []

    for number, data in enumerate(open(path)):

        if (number == 0):
            continue

        lst = data.lower().strip('\n').split('\t')
        ids.append(lst[0])

        question = lst[1].split(' ')
        answer = lst[2]
        possibleAns = [l.split(' ') for l in lst[3:]]

        # append to q a list of np.arrays every word in question
        wordsEmb = []
        for word in question:
            word = word.strip('.').strip('?').strip('!').strip(',')
            if word not in stopWords:
                wordsEmb.append(getVec4Word(model,word))
        q.append(wordsEmb)

        # append y a np.array which labels correct answer
        correctAns = np.zeros(4)
        if answer == 'a':
            correctAns[0] = 1
        elif answer == 'b':
            correctAns[1] = 1
        elif answer == 'c':
            correctAns[2] = 1
        elif answer == 'd':
            correctAns[3] = 1
        else:
            print ("error: unexpected answer at question: ", number)
        y.append(correctAns)

        # append to 'a' a list of 4 lists with np.arrays of words in possible answer
        possAnsEmb = []
        for currentAns in possibleAns:
            wordsEmb = []
            for word in currentAns:
                word = word.strip('.').strip('?').strip('!').strip(',')
                wordsEmb.append(getVec4Word(model,word))
            possAnsEmb.append(wordsEmb)
        a.append(possAnsEmb)

    return np.asarray(q), np.asarray(a), np.asarray(y)



def readTestData(model, path = "../Data/validation_set.tsv", rmStopWords = True):

    ids = []
    q = []
    a = []

    if (rmStopWords):
        stopWords = open("../Data/StopWords.txt").read().splitlines()
    else:
        stopWords = []

    for number, data in enumerate(open(path)):

        if (number == 0):
            continue

        lst = data.lower().strip('\n').split('\t')
        ids.append(lst[0])

        question = lst[1].split(' ')
        possibleAns = [l.split(' ') for l in lst[2:]]

        # append to q a list of np.arrays every word in question
        wordsEmb = []
        for word in question:
            word = word.strip('.').strip('?').strip('!').strip(',')
            if word not in stopWords:
                wordsEmb.append(getVec4Word(model,word))
        q.append(wordsEmb)

        # append to 'a' a list of 4 lists with np.arrays of words in possible answer
        possAnsEmb = []
        for currentAns in possibleAns:
            wordsEmb = []
            for word in currentAns:
                word = word.strip('.').strip('?').strip('!').strip(',')
                wordsEmb.append(getVec4Word(model,word))
            possAnsEmb.append(wordsEmb)
        a.append(possAnsEmb)
    
    return np.asarray(q), np.asarray(a), np.asarray(ids)



def trainBoW(q,a,y, seed = 8024, regStrength = 0.001, lRate = 0.02, maxEpochs = 1000,
    modelSize = 100, lDecay = 0.95, updateType = 'SGD', rho = 0.1, Validation = False):

    np.random.seed(seed)

    # symbolic variables
    q_t = T.vector('q_t')
    a_t = T.matrix('a_t')
    y_t = T.vector('y_t')

    # initialize weights (distribution from paper)
    M = theano.shared(np.random.normal(0,0.01,(modelSize,modelSize)))
    b = theano.shared(np.random.normal(0,0.01,4))

    params = [M,b]

    # define graph
    prob = T.nnet.softmax(T.dot(T.dot(M, a_t).transpose(), q_t) + b)
    xent = - y_t * T.log(prob) - (1 - y_t) * T.log((1 - prob))
    loss = xent.sum() + (regStrength/2) * T.sqrt(((M**2).sum() + (b**2).sum()))
    grads = T.grad(loss, params)

    # define the update model
    if (updateType == 'SGD'):

        print ("SGD")
        updates = []
        for parameter, gradient in zip(params,grads):
            updates.append((parameter, parameter - lRate * gradient))

    elif (updateType == 'AdaGrad'):

        print ("AdaGrad")
        updates = []
        for parameter, gradient in zip(params, grads):
            gradient_hist = theano.shared(parameter.get_value() * 0.)
            updates.append((parameter, parameter - (rho / (rho + T.sqrt(gradient_hist))) * lRate * gradient))
            updates.append((gradient_hist, gradient_hist + gradient ** 2))

    else:
        print ("wrong updateType")

    # compile
    train = theano.function(
        inputs = [q_t, a_t, y_t],
        outputs = [prob, loss],
        updates = updates
    )

    indices = np.random.permutation(len(q))
    q,a,y = q[indices], a[indices], y[indices]

    if (Validation):

        validate = theano.function(
        inputs = [q_t, a_t],
        outputs = prob
        )

        # create test and validation set
        n_Train = int(len(q) * 0.9)
        q_Val = q[n_Train:]
        a_Val = a[n_Train:]
        y_Val = y[n_Train:]
        q = q[:n_Train]
        a = a[:n_Train]
        y = y[:n_Train]

        print (len(q_Val), " questions for validation and ", len(q), "for testing")
    else:
        print (len(q), "questions used for training")


    # sum up questions and answers by bag of words method
    for epoch in range(maxEpochs):

        if (updateType != 'AdaGrad'):
            lRate = lRate * (1 - lDecay * epoch / maxEpochs)

        countCorrect = 0
        countCorrectVal = 0

        indices = np.random.permutation(len(q))
        q,a,y = q[indices], a[indices], y[indices]


        for question, answers, labels in zip(q,a,y):

            q_Em = np.asarray(question).mean(axis = 0)
            a_Em = np.asarray([np.asarray(possibleAns).mean(axis = 0) for possibleAns in answers]).transpose()
            y_Em = np.asarray(labels)

            prob, loss = train(q_Em, a_Em, y_Em)

            if (prob.argmax() == y_Em.argmax()):
                countCorrect += 1


        if (Validation):
            for question, answers, labels in zip(q_Val, a_Val, y_Val):

                q_Em = np.asarray(question).mean(axis = 0)
                a_Em = np.asarray([np.asarray(possibleAns).mean(axis = 0) for possibleAns in answers]).transpose()
                y_Em = np.asarray(labels)

                prob = validate(q_Em, a_Em)

                if (prob.argmax() == y_Em.argmax()):
                    countCorrectVal += 1

            print (countCorrect, "/2250 testing and", countCorrectVal, "/250 validation in epoch:", epoch)
        else:
            print (countCorrect, "/2400 correct in epoch:", epoch)

    return M, b



def predictBoW(q, a, M, b, ids):

    # symbolic variables
    q_t = T.vector('q_t')
    a_t = T.matrix('a_t')

    # define graph
    prob = T.tanh(T.dot(T.dot(M, a_t).transpose(), q_t) + b)

    # compile
    predict = theano.function(
    inputs = [q_t, a_t],
    outputs = prob
    )

    answerList = []
    for question, answers, num in zip(q,a,ids):

        q_Em = np.asarray(question).mean(axis = 0)
        a_Em = np.asarray([np.asarray(possibleAns).mean(axis = 0) for possibleAns in answers]).transpose()

        prob = predict(q_Em, a_Em)

        if (prob.argmax() == 0):
            ans = "A"
        elif (prob.argmax() == 1):
            ans = "B"
        elif (prob.argmax() == 2):
            ans = "C"
        elif (prob.argmax() == 3):
            ans = "D"
        else:
            print ("unexpected answer for question: ", num)
            ans = "ERROR"

        answerList.append(ans)

    return answerList



def main():
    # q: list (all questions) of lists(words per question) of modelSizex1d numpy array
    # a: list (all questions) of lists(4 possible answers) of lists(words) of modelSizex1d numpy array
    # y: list (all questions) of 4x1d numpy Array (correct answer)
	
    start_time = time.clock()

    # parameters
    lRate = 1. # around: 0.02 for SGD and 1. for AdaGrad
    regStrength = 0.0011
    maxEpochs = 200 # around 1000 for sgd and 150 for adagrad
    modelSize = 100
    lDecay = 0.95 # only for SGD
    Validation = True
    rho = 0.1 # factor to avoid 0-division
    updateType = 'AdaGrad' # SGD or AdaGrad

    print ("reading in Glove-Model and Data")
    model = gensim.models.Word2Vec.load_word2vec_format('../Data/GloveWiki'+str(modelSize)+'d.txt', binary=False)
    q,a,y = readTrainingData(model)


    print ("training")
    start_training = time.clock()
    M, b = trainBoW(q,a,y,
        lRate = lRate,
        regStrength = regStrength,
        maxEpochs = maxEpochs,
        modelSize = modelSize,
        lDecay = lDecay,
        updateType = updateType,
        Validation = Validation,
        seed = 5555
        )
    mins_training = (time.clock() - start_training) / 60
    print ("training took ", mins_training, " mins" )

    print ("predict")
    q_Test, a_Test, ids = readTestData(model)
    answers = predictBoW(q_Test, a_Test, M, b, ids)


    with open('../Answers_BoW.csv', 'w') as writer:
        writer.write('id,correctAnswer\n')
        for num, ans in zip(ids,answers):
            # writer.write(str(count) + ',"' + '{:.0f}'.format(p) + '"\n')
            writer.write('{},{}\n'.format(num, ans))

    mins_all = (time.clock() - start_time) / 60
    print ("the program took ", mins_all, " mins")



if __name__ == '__main__':
	main()