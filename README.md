# Kaggle AI Challenge #

This is my code for the Kaggle AI Challenge. The goal is to train a model that can pass an 8th graders science test. The program is presented with the question and 4 different answer possibilities from which it has to choose the best.

My primary is goal is not to win the challenge but to implement the models from these two papers and test their performance on the dataset:

### [Deep Learning for Answer Sentence Selection](http://arxiv.org/abs/1412.1632) ###

This paper presents two models which from the word embeddings of question and answers train a Regression Model to compute the semantic similarities. The Model differ in the way they create the "sentence embeddings" from the single word embeddings. 

The Bag of Words Model averages over the words in the question and in each answer possibility. This models works and manages, with just a little fine-tuning to get into the top 25% of the public leaderboard.

The Bigram Model trains a 1D convolution over the word embeddings to calculate the sentence vector. This Model is not yet training correctly, since it does not manage to significantly improve validation scores over a random guess.

Both approaches use pre-trained [GloVe](http://nlp.stanford.edu/projects/glove/) models for the word vectors and are implemented in python and using theano

### [End-To-End Memory Networks](http://arxiv.org/abs/1503.08895) ###

**TODO**