import os
import sys
import time
import math

import gensim

import numpy as np

import theano
import theano.tensor as T

from theano.ifelse import ifelse


'''
TODO
 - implement softmax
 - rewrite main to implement scripting for gridsearch
 - implement softmax for Bigram Model
 - implement minibatches
 - read test data has to implement rmOneWordAnswers and paddOneWordAnswers
'''


def getVec4Word(model, word):

    try:
        vec = model[word]
    except:
        vec = np.zeros(model.vector_size)
    return vec



def readTrainingData(model, path = "../Data/training_set.tsv", rmStopWords = True, paddOneWordAnswers = False):

    ids = []

    q = []
    a = []
    y = []

    if (rmStopWords):
        stopWords = open("../Data/StopWords.txt").read().splitlines()
    else:
        stopWords = []

    for number, data in enumerate(open(path)):

        if (number == 0):
            continue

        lst = data.lower().strip('\n').split('\t')
        ids.append(lst[0])

        question = lst[1].split(' ')
        answer = lst[2]
        possibleAns = [l.split(' ') for l in lst[3:]]

        # append to q a list of np.arrays every word in question
        wordsEmb = []
        for word in question:
            word = word.strip('.').strip('?').strip('!').strip(',')
            if word not in stopWords:
                wordsEmb.append(getVec4Word(model,word))

        # append y a np.array which labels correct answer
        correctAns = np.zeros(4)
        if answer == 'a':
            correctAns[0] = 1
        elif answer == 'b':
            correctAns[1] = 1
        elif answer == 'c':
            correctAns[2] = 1
        elif answer == 'd':
            correctAns[3] = 1
        else:
            print ("error: unexpected answer at question: ", number)

        # append to 'a' a list of 4 lists with np.arrays of words in possible answer
        possAnsEmb = []
        for currentAns in possibleAns:
            ansEmb = []
            for word in currentAns:
                word = word.strip('.').strip('?').strip('!').strip(',')
                ansEmb.append(getVec4Word(model,word))
            possAnsEmb.append(ansEmb)
         
        # padd one word answers so scan works
        if (paddOneWordAnswers):
            for i in range(len(possAnsEmb)):
                if (len(possAnsEmb[i]) == 1):
                    zeros = np.zeros(len(possAnsEmb[i][0]))
                    possAnsEmb[i] = [possAnsEmb[i][0], zeros]

        q.append(wordsEmb)
        a.append(possAnsEmb)
        y.append(correctAns)


    return np.asarray(q), np.asarray(a), np.asarray(y)



def readTestData(model, path = "../Data/validation_set.tsv", rmStopWords = True, paddOneWordAnswers = False):

    ids = []
    q = []
    a = []

    if (rmStopWords):
        stopWords = open("../Data/StopWords.txt").read().splitlines()
    else:
        stopWords = []

    for number, data in enumerate(open(path)):

        if (number == 0):
            continue

        lst = data.lower().strip('\n').split('\t')
        ids.append(lst[0])

        question = lst[1].split(' ')
        possibleAns = [l.split(' ') for l in lst[2:]]

        # append to q a list of np.arrays every word in question
        wordsEmb = []
        for word in question:
            word = word.strip('.').strip('?').strip('!').strip(',')
            if word not in stopWords:
                wordsEmb.append(getVec4Word(model,word))

        # append to 'a' a list of 4 lists with np.arrays of words in possible answer
        possAnsEmb = []
        for currentAns in possibleAns:
            wordsEmb = []
            for word in currentAns:
                word = word.strip('.').strip('?').strip('!').strip(',')
                wordsEmb.append(getVec4Word(model,word))
            possAnsEmb.append(wordsEmb)

        # padd one word answer so scan works
        if (paddOneWordAnswers):

            if len(wordsEmb) == 1:
                zeros = np.zeros(len(wordsEmb[0]))
                wordsEmb = [wordsEmb[0], zeros]

            for i in range(len(possAnsEmb)):
                if (len(possAnsEmb[i]) == 1):
                    zeros = np.zeros(len(possAnsEmb[i][0]))
                    possAnsEmb[i] = [possAnsEmb[i][0], zeros]

        q.append(wordsEmb)
        a.append(possAnsEmb)
    
    return np.asarray(q), np.asarray(a), np.asarray(ids)


def convBigram(s, Tl, Tr, bt):
    # convolution by bigram model -> turns list of embedded words into embedded sentence

    s_scan, scanUpdates = theano.scan(
                    lambda t_0, t_p1, prior_result, Tl, Tr, b: prior_result + T.tanh(T.dot(Tl, t_0) + T.dot(Tr, t_p1) + b),
                    sequences = dict(input = s, taps = [0, 1]),
                    outputs_info = T.zeros_like(bt, dtype = 'float64'),
                    non_sequences = [Tl, Tr, bt]
    )
    s_em = s_scan[-1]

    return s_em


def trainBigram(q, a, y, seed = 8024, regStrength = 0.001, lRate = 1., maxEpochs = 5000,
    modelSize = 100, rho = 0.1, Validation = False, updateType = 'AdaGrad', annealing = 0.99):

    # trains Model with Adagrad on a Softmax probability

    np.random.seed(seed)

    # theano variables
    q_t = T.dmatrix('q_t')
    a_t1, a_t2, a_t3, a_t4 = T.dmatrices('a_t1', 'a_t2', 'a_t3', 'a_t4')
    y_t = T.vector('y_t')
    learning_rate = T.scalar('learning_rate')

    # initialize weights (distribution from paper)
    M = theano.shared(np.random.normal(0,0.01,(modelSize,modelSize)))
    bm = theano.shared(np.random.normal(0,0.01,4))

    Tl = theano.shared(np.random.normal(0,0.01,(modelSize,modelSize)))
    Tr = theano.shared(np.random.normal(0,0.01,(modelSize,modelSize)))
    bt = theano.shared(np.random.normal(0,0.01,modelSize))

    params = [M, bm, Tl, Tr, bt]

    # define graph for Bigram Model
    q_em = convBigram(q_t, Tl, Tr, bt)
    a_em = T.stack(convBigram(a_t1, Tl, Tr, bt), convBigram(a_t2, Tl, Tr, bt), convBigram(a_t3, Tl, Tr, bt), convBigram(a_t4, Tl, Tr, bt))

    # define graph for Question/Answer model
    # prob = T.nnet.softmax(T.dot(T.dot(M, a_t).transpose(), q_t) + b)
    prob = T.nnet.softmax(T.dot(T.dot(M, a_em.transpose()).transpose(), q_em) + bm)
    xent = - y_t * T.log(prob + 1e-10) - (1 - y_t) * T.log((1 - prob + 1e-10))

    L2reg = theano.shared(0.)
    for parameter in params:
        L2reg += T.sum(parameter ** 2)
    loss = xent.sum() + (regStrength/2) * L2reg

    # output the graph as an Image
    #theano.printing.pydotprint(prob, outfile="../logreg_pydotprint_prob.png", var_with_name_simple=True)
    #theano.printing.pydotprint(xent, outfile="../logreg_pydotprint_xent.png", var_with_name_simple=True)  
    #theano.printing.pydotprint(loss, outfile="../logreg_pydotprint_loss.png", var_with_name_simple=True)    

    grads = T.grad(loss, params)

    # define the update model
    if (updateType == 'SGD'):

        print ("SGD")
        updates = []
        for parameter, gradient in zip(params,grads):
            updates.append((parameter, parameter - learning_rate * gradient))

    elif (updateType == 'AdaGrad'):
        
        print ("AdaGrad")
        updates = []
        for parameter, gradient in zip(params, grads):
            gradient_hist = theano.shared(parameter.get_value() * 0.)
            updates.append((parameter, parameter - (rho / (rho + T.sqrt(gradient_hist))) * learning_rate * gradient))
            updates.append((gradient_hist, gradient_hist + gradient ** 2))

    else:
        print ("wrong updateType")

    # compile function
    train = theano.function(
        inputs = [q_t, a_t1, a_t2, a_t3, a_t4, y_t, learning_rate],
        outputs = [prob, xent, loss],
        updates = updates
    )

    # split data for validation
    if (Validation):

        indices = np.random.permutation(len(q))
        q,a,y = q[indices], a[indices], y[indices]

        validate = theano.function(
            inputs = [q_t, a_t1, a_t2, a_t3, a_t4],
            outputs = prob
        )

        # create test and validation set
        n_Train = int(len(q) * 0.9)
        q_Val = q[n_Train:]
        a_Val = a[n_Train:]
        y_Val = y[n_Train:]
        q = q[:n_Train]
        a = a[:n_Train]
        y = y[:n_Train]

        print (len(q_Val), "questions for validation and", len(q), "for testing")
    else:
        print (len(q), "questions used for training")


    # training
    for epoch in range(maxEpochs):

        count = 0
        countCorrect = 0
        countCorrectVal = 0

        indices = np.random.permutation(len(q))
        q,a,y = q[indices], a[indices], y[indices]

        print "    M", M.get_value().mean()
        print "    bm", bm.get_value().mean()
        print "    Tl", Tl.get_value().mean()
        print "    Tr", Tr.get_value().mean()
        print "    bt", bt.get_value().mean()
        

        for question, answers, labels in zip(q,a,y):

            question = np.asarray(question)
            answers = np.asarray(answers)
            labels = np.asarray(labels)

            probs, xent, loss = train(question, answers[0], answers[1], answers[2], answers[3], labels, lRate)
            

            if (probs.argmax() == labels.argmax()):
                countCorrect += 1

        if (Validation):

            for question, answers, labels in zip(q_Val, a_Val, y_Val):

                question = np.asarray(question)
                answers = np.asarray(answers)
                labels = np.asarray(labels)

                probs = validate(question, answers[0], answers[1], answers[2], answers[3])

                if (probs.argmax() == labels.argmax()):
                    countCorrectVal += 1

            print (countCorrect, "/", len(q)," testing and", countCorrectVal, "/", len(q_Val),"validation in epoch:", epoch)
        else:
            print (countCorrect, "correct of", len(q),"in epoch:", epoch)

        if updateType == 'SGD':
            lRate = lRate * annealing


    return M, bm, Tl, Tr, bt



def predictBigram(q, a, M, bm, Tl, Tr, bt):

    # symbolic variables
    q_t = T.matrix('q_t')
    a_t1, a_t2, a_t3, a_t4 = T.dmatrices('a_t1', 'a_t2', 'a_t3', 'a_t4')

    # define graph
    
    q_em = convBigram(q_t, Tl, Tr, bt)
    a_em = T.stack(convBigram(a_t1, Tl, Tr, bt), convBigram(a_t2, Tl, Tr, bt), convBigram(a_t3, Tl, Tr, bt), convBigram(a_t4, Tl, Tr, bt))

    prob = T.nnet.softmax(T.dot(T.dot(M, a_em.transpose()).transpose(), q_em) + bm)

    # compile
    predict = theano.function(
       inputs = [q_t, a_t1, a_t2, a_t3, a_t4],
       outputs = prob
    )

    # predict
    answerList = []
    for question, answers in zip(q,a):
        question = np.asarray(question)
        answers = np.asarray(answers)

        probs = predict(question, answers[0], answers[1], answers[2], answers[3])

        if (probs.argmax() == 0):
            ans = "A"
        elif (probs.argmax() == 1):
            ans = "B"
        elif (probs.argmax() == 2):
            ans = "C"
        elif (probs.argmax() == 3):
            ans = "D"
        else:
            print ("unexpected answer for question: ", num)
            ans = "ERROR"

        answerList.append(ans)

    return answerList



def main():
    # q: list (all questions) of lists(words per question) of modelSizex1d numpy array
    # a: list (all questions) of lists(4 possible answers) of lists(words) of modelSizex1d numpy array
    # y: list (all questions) of 4x1d numpy Array (correct answer)
	
    start_time = time.clock()

    # parameters for reading in Data
    rmStopWords = True
    rmOneWordAnswers = False
    paddOneWordAnswers = True

    # parameters for training
    lRate = .1 # around: 0.02 for softmax, 0.0015 for tanh, 0.1 for AdaGrad --> 1. oscillates
    annealing = 1.
    regStrength = 0.0000
    maxEpochs = 300 # around 1000 for sgd and 150 for adagrad
    modelSize = 50 # does not seem to work yet with 300
    Validation = True
    rho = 0.1 # prevents dividing through zero and mulitplies first lrate by 1 / rho
    updateType = 'AdaGrad'

    print "lRate:", lRate, "regStrength", regStrength

    print ("reading in Glove-Model and Data")
    model = gensim.models.Word2Vec.load_word2vec_format('../Data/GloveWiki'+str(modelSize)+'d.txt', binary=False)
    q,a,y = readTrainingData(model, rmStopWords = rmStopWords, paddOneWordAnswers = paddOneWordAnswers)

    print ("training")

    M, bm, Tl, Tr, bt = trainBigram(q,a,y,
        lRate = lRate,
        regStrength = regStrength,
        maxEpochs = maxEpochs,
        modelSize = modelSize,
        Validation = Validation,
        rho = rho,
        seed = 5555,
        updateType = updateType,
        annealing = annealing
        )

    print ("predict")
    q_Test, a_Test, ids = readTestData(model, rmStopWords = rmStopWords, paddOneWordAnswers = paddOneWordAnswers)

    answers = predictBigram(q_Test, a_Test, M, bm, Tl, Tr, bt)

    with open('../Answers_Bigram.csv', 'w') as writer:
        writer.write('id,correctAnswer\n')
        for num, ans in zip(ids,answers):
            # writer.write(str(count) + ',"' + '{:.0f}'.format(p) + '"\n')
            writer.write('{},{}\n'.format(num, ans))

    mins_all = (time.clock() - start_time) / 60
    print ("the program took", mins_all, "mins")



if __name__ == '__main__':
	main()

# (1719, '/', 2250, ' testing and', 74, '/', 250, 'validation in epoch:', 199)